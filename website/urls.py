from django.conf.urls import patterns, include, url
from django.contrib import admin
from main.views import index, about, tmatrix

urlpatterns = [
    # Examples:
    # url(r'^$', 'website.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', index),
    url(r'^about/$', about),
    url(r'^tmatrix/', tmatrix),
    url(r'^admin/', include(admin.site.urls)),
]
