'use strict';
console.log('hiii');

function onFormSubmit() {
    let form = document.getElementById('tmatrix-form');
    let inputs = form.getElementsByTagName('input');
    for (let i = 0; i < inputs.length; i++) {
        let input = inputs[i];
        if (input.type === "submit") {
            continue
        }
        if (input.value === "") {
            input.disabled = true;
        }
        console.log(input.type);
    }
    console.log("form submitted");
}

window.onload = function () {
    let form = document.getElementById('tmatrix-form');
    form.onsubmit = onFormSubmit;
};

