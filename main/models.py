from django.db import models


class Category(models.Model):
    category = models.CharField(max_length=50, primary_key=True)


class BlogPost(models.Model):
    categories = models.ManyToManyField(Category)
    title = models.CharField(max_length=100, unique=True)
    content = models.TextField(default='')
    date_published = models.DateTimeField()

