from django.shortcuts import render
from main.forms import TmatrixForm
import pytmatrix.tmatrix


# Create your views here.
def index(request):
    return render(request, 'index.html')


def about(request):
    return render(request, 'about.html')


def tmatrix(request):
    params = {k: v for k, v in request.GET.items() if v}
    if params:
        form = TmatrixForm(request.GET)
        scatterer = pytmatrix.tmatrix.Scatterer(**params)
        S, Z = scatterer.get_SZ()
        print(S)
    else:
        form = TmatrixForm()
        S = []
        Z = []
    return render(request, 'tmatrix.html', {'form': form, 'S': S, 'Z': Z})

