from django import forms

class TmatrixForm(forms.Form):
    radius = forms.FloatField(
        label='drop radius (mm)',
        required=True,
        min_value=1E-10,
        )
    wavelength = forms.FloatField(
        label= 'The wavelength of incident light (same units as axi)',
        required=True,
        min_value=1E-10,
    )
    m_real = forms.FloatField(
        label='The real part of the complex refractive index',
        required=False,
    )
    m_imag = forms.FloatField(
        label='The imaginary part of the complex refractive index',
        required=False,
    )
    axis_ratio = forms.FloatField(
        label='The horizontal-to-rotational axis ratio',
        required=False,
    )
    alpha = forms.FloatField(
        label='The Euler angles of the particle orientation (degrees)',
        required=False,
        min_value=0,
        max_value=360,
    )
    beta = forms.FloatField(
        label='The Euler angles of the particle orientation (degrees)',
        required=False,
        min_value=0,
        max_value=360,
    )
    thet0 = forms.FloatField(
        label='The zenith angle of the incident radiation (degrees)',
        required=False,
        min_value=0,
        max_value=360,
    )
    thet = forms.FloatField(
        label='The zenith angle of the scattered radiation (degrees)',
        required=False,
        min_value=0,
        max_value=360,
    )
    phi0 = forms.FloatField(
        label='The azimuth angle of the scattered radiation (degrees)',
        required=False,
        min_value=0,
        max_value=360,
    )
    phi = forms.FloatField(
        label='The azimuth angle of the scattered radiation (degrees)',
        required=False,
        min_value=0,
        max_value=360,
    )
    Kw_sqr = forms.FloatField(
        label='The squared reference water dielectric factor',
        required=False,
    )
   # orient: The function to use to compute the scattering properties.
   #     Should be one of the orientation module methods (orient_single,
   #     orient_averaged_adaptive, orient_averaged_fixed).
   # or_pdf: Particle orientation PDF for orientational averaging.
   # n_alpha: Number of integration points in the alpha Euler angle.
   # n_beta: Number of integration points in the beta Euler angle.
   # psd_integrator: Set this to a PSDIntegrator instance to enable size
   #     distribution integration. If this is None (default), size
   #     distribution integration is not used. See the PSDIntegrator
   #     documentation for more information.
   # psd: Set to a callable object giving the PSD value for a given
   #     diameter (for example a GammaPSD instance); default None. Has no
   #     effect if psd_integrator is None.

