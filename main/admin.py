from django.contrib import admin
from main.models import BlogPost, Category


class CategoryInline(admin.TabularInline):
    model = Category
    extra = 3


# Register your models here.
admin.site.register(BlogPost)
