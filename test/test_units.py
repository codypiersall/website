from units.models import Quantity, Measurement, Unit

meter = Unit.objects.get(name='meter')
foot = Unit.objects.get(name='foot')
inch = Unit.objects.get(name='inch')


def test_conversions():

    one_meter = Measurement(unit=meter, value=1)
    still_one_meter = one_meter.to_other(meter)
    assert one_meter.value == still_one_meter.value
    to_feet = one_meter.to_other(foot)
    print(to_feet.value)
    assert to_feet.value == one_meter.value / .3048

    pass
