import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'website.settings')

import django
django.setup()
