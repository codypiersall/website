#! /bin/sh
# Push the website to AWS host.

sass --update static
find . -name "*.css" -o -name "*.html" -o -name "*.py" -o -name "*.js" | \
    tar -c -T - | \
    ssh aws "
    cd ~;
    rm -rf website;
    mkdir website;
    tar -C website -xf -;
    sudo service apache2 restart;
"

